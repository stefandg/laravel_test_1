<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\StoreItemRequest;
use App\Http\Requests\EditItemRequest;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $search = Input::get('search');

        if (filled($search)) {
            $items = Item::where('name', $search)->latest()->paginate(6);
        }
        else {
            $items = Item::latest()->paginate(6);
        }

        return view('item.index', compact('items','search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('item.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreItemRequest $request)
    {
        $item = new Item;

        $item->name=$request->name;
        $item->description=$request->description;
        $item->category=$request->category;
        $item->material=$request->material;
        if($request->has('available')) {
            $item->available=$request->available;
        }else{
            $item->available= 0;
        }
        $item->save();

        return redirect()->route('item.index')->with('success','create success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        return view('item.edit',compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(EditItemRequest $request, Item $item)
    {
        $item->name=$request->name;
        $item->description=$request->description;
        $item->category=$request->category;
        $item->material=$request->material;
        if($request->has('available')) {
            $item->available=$request->available;
        }else{
            $item->available= 0;
        }
        $item->save();

        return redirect()->route('item.index')->with('message','update success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        $item->forceDelete();
        return redirect()->route('item.index')->with('message','delete success');
    }

    public function trash(Item $item)
    {
        $item->delete();
        return redirect()->route('item.index')->with('message','trash success');
    }
}
