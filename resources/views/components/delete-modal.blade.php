<div aria-hidden="true" aria-labelledby="{{ $id }}Label"
     class="modal fade" id="{{ $id }}" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="{{ $id }}Label">
                    {{ __('are_you_sure') }}
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal"
                        type="button"><span aria-hidden="true"> &times;</span></button>
            </div>
            <div class="modal-body">
                <h6>
                    {{ $slot }}
                </h6>

                <form action="{{ $route }}" method="POST">
                    @csrf
                    @method('DELETE')

                    <button class="btn btn-primary" type="submit">{{ __('delete') }}</button>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" data-dismiss="modal" type="button">
                    {{ __('close') }}
                </button>
            </div>
        </div>
    </div>
</div>