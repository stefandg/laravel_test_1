<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>Item</title>

      <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      
      <style>
          html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
          }

          .full-height {
            height: 100vh;
          }

          .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
          }

          .content {
            text-align: center;
          }
      </style>
    </head>
    <body>
    <div class="flex-center position-ref full-height">
      <div class="container">
        <div>
        <a href="{{ route('item.index') }}" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true">go back</a>
        </div>
        <br>
        <form action="{{ route('item.update' , $item->id) }}" method="POST">
            @csrf
            @method('PUT')

            @if($errors->any())
                @foreach($errors->all() as $error)
                  <div class="alert alert-danger">
                    {{ $error }}
                  </div>
                @endforeach
            @endif
            <div class="form-group">
              <label for="name">Item name</label>
              <input type="text" name="name" id="name" class="form-control" value="{{ old('name', $item->name) }}">
            </div>
            <div class="form-group">
                  <label for="description"> description</label>
                  <textarea name="description" id="description" class="form-control">{{ old('description', $item->description) }}</textarea>
            </div>
            <div class="form-group">
                  <label for="category"> category</label>
                  <select name="category" class="form-control">
                    <option value="category1" {{ (old('category', $item->category) == 'category1' ? 'selected' : '') }}> category 1 </option>
                    <option value="category2" {{ (old('category', $item->category) == 'category2' ? 'selected' : '') }}> category 2 </option>
                  </select>
            </div>
            <div class="form-group">
              <label for="material"> material</label><br>
                <input type="radio" name="material" value="glass" {{ old('material', $item->material )== "glass" ? 'checked' : '' }} > glass<br>
                <input type="radio" name="material" value="metal" {{ old('material', $item->material )== "metal" ? 'checked' : '' }} > metal<br>  
                <input type="radio" name="material" value="plastic" {{ old('material', $item->material )== "plastic" ? 'checked' : '' }} > plastic<br> 
            </div>
            <div class="form-check">
              <input type="checkbox"  name="available" class="form-check-input" value="1" id="available" @if(old('available', $item->available)) checked @endif>
              <label class="form-check-label" for="available"  >available</label>
            </div>
            <br>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>    
      </div>  
      </div>
    </body>
</html>