<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                padding-top: 100px;
                display: flex;
                justify-content: center;
            }

            .content {
                text-align: center;
            }


            
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="container">
                    <a href="{{ route('item.create') }}" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true">Create Item</a>
                </div>
                <div class="container-fluid">
                <br><br>
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <form action="" method="GET" class="form-inline">
                    <input class="form-control" placeholder="name" type="text" name="search" value="{{ $search }}">
                    <input class="btn btn-primary" type="submit" value="search">
                </form>
                <br>
                <table id="selectedColumn" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="th-sm">Name </th>
                            <th class="th-sm">description </th>
                            <th class="th-sm">material </th>
                            <th class="th-sm">category </th>
                            <th class="th-sm">available</th>
                            <th class="th-sm">edit</th>
                            <th class="th-sm">trash</th>
                            <th class="th-sm">delete</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->description }}</td>
                            <td>{{ $item->material }}</td>
                            <td>{{ $item->category }}</td>
                            <td>{{ $item->available == 1 ? "yes" : "no" }}</td>
                            <td><a class="btn btn-info" href="{{ route('item.edit', $item->id) }}" role="button">edit</a></td>
                            <td>
                                <form method="POST" action="{{ route('item.trash', $item->id) }}">
                                        @csrf
                                        @method('DELETE')
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-warning" value="Trash">
                                        </div>
                                    </form>
                            </td>
                            <td>  
                                <form method="POST" action="{{ route('item.destroy', $item->id) }}">
                                    @csrf
                                    @method('DELETE')
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-danger" value="Delete">
                                    </div>
                                </form>     
                            </td>
                        </tr>
                    @endforeach    
                    </tbody>
                </table>
            </div>
        {{ $items->links() }}
        </div>
    </body>
</html>
