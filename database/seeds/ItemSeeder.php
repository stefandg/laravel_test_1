<?php

use App\Item;
use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $item = new Item;
        $item->name = 'item 1';
        $item->description = 'item 1';
        $item->available = true;
        $item->category = 'category1';
        $item->material = 'glass';
        $item->save();

        $item = new Item;
        $item->name = 'item 2';
        $item->description = 'item 2';
        $item->available = false;
        $item->category = 'category2';
        $item->material = 'plastic';
        $item->save();

        $item = new Item;
        $item->name = 'item 3';
        $item->description = 'item 3';
        $item->available = true;
        $item->category = 'category1';
        $item->material = 'metal';
        $item->save();

        $item = new Item;
        $item->name = 'item 4';
        $item->description = 'item 4';
        $item->available = false;
        $item->category = 'category2';
        $item->material = 'glass';
        $item->save();

        $item = new Item;
        $item->name = 'item 5';
        $item->description = 'item 5';
        $item->available = true;
        $item->category = 'category1';
        $item->material = 'plastic';
        $item->save();

        $item = new Item;
        $item->name = 'item 6';
        $item->description = 'item 6';
        $item->available = false;
        $item->category = 'category2';
        $item->material = 'metal';
        $item->save();

        $item = new Item;
        $item->name = 'item 7';
        $item->description = 'item 7';
        $item->available = true;
        $item->category = 'category1';
        $item->material = 'glass';
        $item->save();
    }
}
